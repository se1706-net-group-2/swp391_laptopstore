USE [master]
GO
/****** Object:  Database [Computer_Shop]    Script Date: 5/13/2023 6:48:10 PM ******/
CREATE DATABASE [Computer_Shop]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Computer_Shop', FILENAME = N'D:\sql\MSSQL15.MSSQLSERVER\MSSQL\DATA\Computer_Shop.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Computer_Shop_log', FILENAME = N'D:\sql\MSSQL15.MSSQLSERVER\MSSQL\DATA\Computer_Shop_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Computer_Shop] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Computer_Shop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Computer_Shop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Computer_Shop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Computer_Shop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Computer_Shop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Computer_Shop] SET ARITHABORT OFF 
GO
ALTER DATABASE [Computer_Shop] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Computer_Shop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Computer_Shop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Computer_Shop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Computer_Shop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Computer_Shop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Computer_Shop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Computer_Shop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Computer_Shop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Computer_Shop] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Computer_Shop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Computer_Shop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Computer_Shop] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Computer_Shop] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Computer_Shop] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Computer_Shop] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Computer_Shop] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Computer_Shop] SET RECOVERY FULL 
GO
ALTER DATABASE [Computer_Shop] SET  MULTI_USER 
GO
ALTER DATABASE [Computer_Shop] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Computer_Shop] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Computer_Shop] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Computer_Shop] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Computer_Shop] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Computer_Shop] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'Computer_Shop', N'ON'
GO
ALTER DATABASE [Computer_Shop] SET QUERY_STORE = OFF
GO
USE [Computer_Shop]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[AdminID] [int] IDENTITY(1,1) NOT NULL,
	[AdminName] [nvarchar](100) NULL,
	[Address] [nvarchar](200) NULL,
	[Phone] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Password] [varchar](100) NULL,
	[RoleID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[AdminID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AdminRole]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AdminRole](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](40) NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Brand]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Brand](
	[BrandID] [int] IDENTITY(1,1) NOT NULL,
	[BrandName] [varchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[BrandID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryID] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[UserID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Content] [ntext] NULL,
	[CreateDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC,
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Error]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Error](
	[ErrorID] [int] IDENTITY(1,1) NOT NULL,
	[ErrorName] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[ErrorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Guarantee]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guarantee](
	[GuaranteeID] [int] IDENTITY(1,1) NOT NULL,
	[ProductID] [int] NULL,
	[UserID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[GuaranteeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[NewsID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Content] [ntext] NULL,
	[NewsType] [int] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[Image] [nvarchar](max) NULL,
	[Video] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[NewsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NewsType]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsType](
	[NewsTypeID] [int] IDENTITY(1,1) NOT NULL,
	[NewsTypeName] [nvarchar](60) NULL,
PRIMARY KEY CLUSTERED 
(
	[NewsTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[OrderDate] [datetime] NULL,
	[ShipDate] [datetime] NULL,
	[TotalPrice] [money] NULL,
	[Status] [bit] NULL,
	[PaymentType] [nvarchar](100) NULL,
	[ShipAddress] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetails]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetails](
	[ProductID] [int] NOT NULL,
	[OrderID] [int] NOT NULL,
	[Quantity] [int] NULL,
	[Price] [money] NULL,
	[Discount] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC,
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](100) NULL,
	[BrandID] [int] NULL,
	[CategoryID] [int] NULL,
	[Description] [nvarchar](max) NULL,
	[UnitPrice] [money] NULL,
	[Discount] [int] NULL,
	[Img] [varchar](max) NULL,
	[UnitInStock] [int] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[Guarantee] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RepairBill]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RepairBill](
	[BillID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[TotalPrice] [money] NULL,
	[Address] [nvarchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[BillID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RepairBillDetails]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RepairBillDetails](
	[BillID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[ErrorID] [int] NOT NULL,
	[Quantity] [int] NULL,
	[Price] [money] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC,
	[BillID] ASC,
	[ErrorID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 5/13/2023 6:48:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](100) NULL,
	[Address] [nvarchar](200) NULL,
	[Phone] [varchar](50) NULL,
	[Email] [varchar](100) NULL,
	[Password] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([AdminID], [AdminName], [Address], [Phone], [Email], [Password], [RoleID]) VALUES (1, N'Nguyễn Văn A', N'Số 110 phố Nguyễn Thái Học, Phường Điện Biên, Quận Ba Đình, Thành phố Hà Nội
', N'0981620906', N'nguyenvana@gmail.com', N'111111', 1)
INSERT [dbo].[Admin] ([AdminID], [AdminName], [Address], [Phone], [Email], [Password], [RoleID]) VALUES (2, N'Nguyễn Văn B', N'Số 219 Ỷ La, Phường Dương Nội, Quận Hà Đông, Thành phố Hà Nội', N'0919786333', N'nguyenvanb@gmail.com', N'222222', 2)
INSERT [dbo].[Admin] ([AdminID], [AdminName], [Address], [Phone], [Email], [Password], [RoleID]) VALUES (3, N'Nguyễn Văn C', N'Số 178 Đường Nước Phần Lan, Phường Tứ Liên, Quận Tây Hồ, Thành phố Hà Nội
', N'0398227289', N'nguyenvanc@gmail.com', N'333333', 3)
INSERT [dbo].[Admin] ([AdminID], [AdminName], [Address], [Phone], [Email], [Password], [RoleID]) VALUES (4, N'Nguyễn Văn D', N'Số 64,66 phố Bạch Mai, Phường Cầu Dền, Quận Hai Bà Trưng, Thành phố Hà Nội
', N'0397663667', N'nguyenvand@gmail.com', N'444444', 4)
INSERT [dbo].[Admin] ([AdminID], [AdminName], [Address], [Phone], [Email], [Password], [RoleID]) VALUES (5, N'Nguyễn Văn E', N'Lô C1-5, Khu đô thị Cầu Diễn, Phường Phú Diễn, Quận Bắc Từ Liêm, Thành phố Hà', N'0982367860', N'nguyenvane@gmail.com', N'555555', 5)
INSERT [dbo].[Admin] ([AdminID], [AdminName], [Address], [Phone], [Email], [Password], [RoleID]) VALUES (7, N'Nguyễn Văn F', N'Lầu 1, tòa nhà 117-119 Trần Nguyên Đán, Phường 3, Quận Bình Thạnh, Thành phố Hồ Chí Minh
', N'0937133138', N'nguyenvanf@gmail.com', N'666666', 1)
INSERT [dbo].[Admin] ([AdminID], [AdminName], [Address], [Phone], [Email], [Password], [RoleID]) VALUES (8, N'Nguyễn Văn G', N'722 Ngô Quyền, Phường An Hải Bắc, Quận Sơn Trà, Thành phố Đà Nẵng
', N'0981333333', N'nguyenvang@gmail.com', N'777777', 2)
INSERT [dbo].[Admin] ([AdminID], [AdminName], [Address], [Phone], [Email], [Password], [RoleID]) VALUES (9, N'Nguyễn Văn H', N'1755 Nguyễn Tất Thành, Phường Thanh Khê Tây, Quận Thanh Khê, Thành phố Đà Nẵng', N'0392222222', N'nguyenvanh@gmail.com', N'888888', 3)
INSERT [dbo].[Admin] ([AdminID], [AdminName], [Address], [Phone], [Email], [Password], [RoleID]) VALUES (10, N'Nguyễn Văn I', N'09 Cửu Long, Phường 2, Quận Tân Bình, Thành phố Hồ Chí Minh
', N'0987123456', N'nguyenvani@gmail.com', N'999999', 4)
SET IDENTITY_INSERT [dbo].[Admin] OFF
GO
SET IDENTITY_INSERT [dbo].[AdminRole] ON 

INSERT [dbo].[AdminRole] ([RoleID], [RoleName]) VALUES (1, N'Admin')
INSERT [dbo].[AdminRole] ([RoleID], [RoleName]) VALUES (2, N'Admin News')
INSERT [dbo].[AdminRole] ([RoleID], [RoleName]) VALUES (3, N'Admin Product')
INSERT [dbo].[AdminRole] ([RoleID], [RoleName]) VALUES (4, N'Admin Seller')
INSERT [dbo].[AdminRole] ([RoleID], [RoleName]) VALUES (5, N'Admin User')
SET IDENTITY_INSERT [dbo].[AdminRole] OFF
GO
SET IDENTITY_INSERT [dbo].[Brand] ON 

INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (1, N'HP')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (2, N'Dell')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (3, N'ASUS')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (4, N'ACER')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (5, N'LENOVO')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (6, N'MSI')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (7, N'LG')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (8, N'GIGABYTE')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (9, N'Apple')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (10, N'Akko')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (11, N'Logitech')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (12, N'Razer')
INSERT [dbo].[Brand] ([BrandID], [BrandName]) VALUES (13, N'Cosair')
SET IDENTITY_INSERT [dbo].[Brand] OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (1, N'Laptop')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (2, N'Màn hình')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (3, N'Chuột')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (4, N'Bàn phím')
INSERT [dbo].[Category] ([CategoryID], [CategoryName]) VALUES (5, N'Tai nghe')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
INSERT [dbo].[Comment] ([UserID], [ProductID], [Content], [CreateDate]) VALUES (1, 1, N'Sản phẩm dùng khá tốt.', CAST(N'2022-02-16T09:55:00.000' AS DateTime))
INSERT [dbo].[Comment] ([UserID], [ProductID], [Content], [CreateDate]) VALUES (2, 1, N'Mẫu mã rất đẹp.', CAST(N'2022-02-21T09:55:00.000' AS DateTime))
INSERT [dbo].[Comment] ([UserID], [ProductID], [Content], [CreateDate]) VALUES (4, 3, N'Sản phẩm này chưa tốt lắm', CAST(N'2022-02-15T09:55:00.000' AS DateTime))
INSERT [dbo].[Comment] ([UserID], [ProductID], [Content], [CreateDate]) VALUES (1, 4, N'Sản phẩm quá tuyệt vời.', CAST(N'2022-02-15T09:55:00.000' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[Error] ON 

INSERT [dbo].[Error] ([ErrorID], [ErrorName]) VALUES (1, N'Lỗi ổ cứng')
INSERT [dbo].[Error] ([ErrorID], [ErrorName]) VALUES (2, N'Lỗi màn hình bể')
INSERT [dbo].[Error] ([ErrorID], [ErrorName]) VALUES (3, N'Nhiễm virut')
INSERT [dbo].[Error] ([ErrorID], [ErrorName]) VALUES (4, N'Lỗi màn hình đen')
INSERT [dbo].[Error] ([ErrorID], [ErrorName]) VALUES (5, N'Lỗi sọc màn hình, đốm sáng')
INSERT [dbo].[Error] ([ErrorID], [ErrorName]) VALUES (6, N'Lỗi RAM')
INSERT [dbo].[Error] ([ErrorID], [ErrorName]) VALUES (7, N'Mainboard bị hư hỏng, lỗi')
INSERT [dbo].[Error] ([ErrorID], [ErrorName]) VALUES (8, N'Tản nhiệt bị hỏng')
INSERT [dbo].[Error] ([ErrorID], [ErrorName]) VALUES (9, N'Lỗi pin')
SET IDENTITY_INSERT [dbo].[Error] OFF
GO
SET IDENTITY_INSERT [dbo].[Guarantee] ON 

INSERT [dbo].[Guarantee] ([GuaranteeID], [ProductID], [UserID], [StartDate], [EndDate]) VALUES (1, 1, 1, CAST(N'2022-02-15T09:55:00.000' AS DateTime), CAST(N'2024-02-15T09:55:00.000' AS DateTime))
INSERT [dbo].[Guarantee] ([GuaranteeID], [ProductID], [UserID], [StartDate], [EndDate]) VALUES (2, 1, 3, CAST(N'2022-02-05T16:00:00.000' AS DateTime), CAST(N'2024-02-05T16:00:00.000' AS DateTime))
INSERT [dbo].[Guarantee] ([GuaranteeID], [ProductID], [UserID], [StartDate], [EndDate]) VALUES (3, 2, 2, CAST(N'2022-06-21T15:12:00.000' AS DateTime), CAST(N'2024-06-21T15:12:00.000' AS DateTime))
INSERT [dbo].[Guarantee] ([GuaranteeID], [ProductID], [UserID], [StartDate], [EndDate]) VALUES (4, 3, 1, CAST(N'2022-05-16T09:55:00.000' AS DateTime), CAST(N'2024-05-16T09:55:00.000' AS DateTime))
INSERT [dbo].[Guarantee] ([GuaranteeID], [ProductID], [UserID], [StartDate], [EndDate]) VALUES (5, 4, 1, CAST(N'2022-02-15T09:55:00.000' AS DateTime), CAST(N'2024-02-15T09:55:00.000' AS DateTime))
INSERT [dbo].[Guarantee] ([GuaranteeID], [ProductID], [UserID], [StartDate], [EndDate]) VALUES (6, 4, 4, CAST(N'2022-02-15T19:45:00.000' AS DateTime), CAST(N'2024-02-15T19:45:00.000' AS DateTime))
INSERT [dbo].[Guarantee] ([GuaranteeID], [ProductID], [UserID], [StartDate], [EndDate]) VALUES (7, 4, 1, CAST(N'2022-05-16T09:55:00.000' AS DateTime), CAST(N'2024-05-16T09:55:00.000' AS DateTime))
INSERT [dbo].[Guarantee] ([GuaranteeID], [ProductID], [UserID], [StartDate], [EndDate]) VALUES (8, 5, 4, CAST(N'2022-02-15T19:45:00.000' AS DateTime), CAST(N'2024-02-15T19:45:00.000' AS DateTime))
INSERT [dbo].[Guarantee] ([GuaranteeID], [ProductID], [UserID], [StartDate], [EndDate]) VALUES (9, 6, 3, CAST(N'2022-02-05T16:00:00.000' AS DateTime), CAST(N'2024-02-05T16:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Guarantee] OFF
GO
SET IDENTITY_INSERT [dbo].[News] ON 

INSERT [dbo].[News] ([NewsID], [Title], [Content], [NewsType], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Image], [Video]) VALUES (1, N'Trên tay HP Pavilion X360 14 ek1049TU: Intel Core i5 Gen 13th mạnh mẽ, bản lề xoay gập 360 độ linh hoạt', N'Về tổng thể, như bao mẫu laptop cao cấp dành cho doanh nhân/ doanh nghiệp trên thị trường hiện nay thì điểm nhấn đầu tiên mà HP Pavilion X360 14 ek1049TU làm mình ấn tượng chính là một thân hình mỏng nhẹ. Cụ thể thì thiết bị chỉ dày 19.9 mm và cân nặng khoảng 1.53 kg, rất là lý tưởng để chúng ta có thể cho vào trong túi xách hoặc balo mà mang đi làm việc ở khắp nơi.Ngoài ra, HP Pavilion X360 14 ek1049TU có khung sườn được chế tạo từ chất liệu kim loại nhằm mang đến vẻ đẹp, sang trọng cũng như mang đến cho người dùng một cảm giác cầm nắm chắc chắn, cứng cáp. Đồng thời, phần mặt A và D của máy được làm từ nhựa và mức độ hoàn thiện rất tốt nên mình không cảm thấy ọp ẹp trong suốt quá trình sử dụng. Thêm vào đó, trên mặt A của HP Pavilion X360 14 ek1049TU tuy đơn giản nhưng nhờ vào sự hiện diện của logo HP hình tròn cực quen thuộc đã giúp tổng thể của thiết bị trở nên nổi bật hơn.Không chỉ có thiết kế mỏng nhẹ, HP Pavilion X360 14 ek1049TU còn sở hữu phần bản lề đặc biệt với khả năng xoay gập màn hình 360 độ, giúp chúng ta có thể lật ngược lại mà sử dụng máy như một chiếc tablet bên cạnh vai trò là một chiếc laptop thông thường. Đồng thời, HP còn trang bị cho máy một chiếc bút cảm ứng đi kèm để người dùng có thể vẽ hoặc ghi chú nhanh tiện lợi hơn.Không chỉ có thiết kế mỏng nhẹ, HP Pavilion X360 14 ek1049TU còn sở hữu phần bản lề đặc biệt với khả năng xoay gập màn hình 360 độ, giúp chúng ta có thể lật ngược lại mà sử dụng máy như một chiếc tablet bên cạnh vai trò là một chiếc laptop thông thường. Đồng thời, HP còn trang bị cho máy một chiếc bút cảm ứng đi kèm để người dùng có thể vẽ hoặc ghi chú nhanh tiện lợi hơn.Tuy sở hữu một thân hình mỏng gọn là thế nhưng HP Pavilion X360 14 ek1049TU vẫn được trang bị đa dạng các cổng kết nối đi kèm để bạn sử dụng những thiết bị ngoại vi không thể thiếu như: Bàn phím rời, chuột, tai nghe, màn hình,... Cụ thể thì ở phía cạnh trái của máy, chúng ta có thể thấy lần lượt là cổng HDMI, cổng USB Type-C và cổng tai nghe 3.5 mm. Trong khi đó ở cạnh phải của thiết bị chứa cổng sạc, hai cổng USB-A 3.2 và khe đọc thẻ nhớ MicroSD.Vì mang ngoại hình nhỏ gọn nên để tối ưu khả năng trải nghiệm, HP Pavilion X360 14 ek1049TU được trang bị bàn phím chiclet với layout TKL thường rất phổ biến trên các laptop doanh nhân. Theo trải nghiệm nhanh của mình thì các phím có hành trình vừa phải, hành trình phím tốt và khoảng cách giữa các phím được phân bố hợp lý nên mình ít khi gặp phải tình trạng gõ nhầm. Hơn nữa, hệ thống bàn phím này còn được tích hợp đèn nền nên sẽ rất hữu dụng cho các bạn thường sử dụng laptop trong điều kiện ánh sáng hạn chế.Ngoài ra, bàn di chuột của HP Pavilion X360 14 ek1049TU tuy khá nhỏ nhắn nhưng vẫn cho lại cảm giác vuốt chạm tốt, mượt mà và tốc độ phản hồi nhanh. Phần chiếu để tay có kích thước vừa phải, cho phép chúng ta có không gian kê tay thoải mái khi làm việc.', 2, 1, NULL, CAST(N'2021-05-13T09:55:00.000' AS DateTime), NULL, N'news1.jpg', NULL)
INSERT [dbo].[News] ([NewsID], [Title], [Content], [NewsType], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Image], [Video]) VALUES (2, N'TUẦN LỄ ASUS: VÀO ÔM KHUYẾN MẠI', N'1. Tên chương trình: TUẦN LỄ ASUS: VÀO ÔM KHUYẾN MẠI
2. Thời gian diễn ra: Từ 08/05/2023 – 14/05/2023
3. Nội dung chương trình:
 An Phát dành tặng quý khách hàng yêu thích dòng Laptop Asus Gaming deal ngon giật mình, giảm giá đến nửa triệu cùng phần quà giới hạn đặc biệt đến từ chính hãng Asus.

CỤ THỂ:  Nhận ngay Voucher 500k giảm giá trên hóa đơn (không áp dụng cho mã NBAS1151 và NBAS1111). Tặng 01 Áo thun ASUS ROG phiên bản giới hạn trên mỗi đơn hàng. Còn chờ gì mà chưa ghé G2Com để mua ngay 1 em Laptop Asus Gaming thôi nào!!', 2, 2, NULL, CAST(N'2021-05-13T09:55:00.000' AS DateTime), NULL, N'news2.jpg', NULL)
INSERT [dbo].[News] ([NewsID], [Title], [Content], [NewsType], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Image], [Video]) VALUES (3, N'Chuột Chơi Game Sở Hữu Switch Bất Tử Với Giá Chỉ 300 Nghìn', N'Sau thành công hoành tráng đến từ hai phiên bản chuột chơi game E-DRA EM6102 Wired (có dây) và E-DRA EM6102 Wireless (không dây), hãng Gaming Gear nổi tiếng Việt Nam lại tiếp tục cho ra đời phiên bản “cao thủ”, sở hữu công tắc quang học gần như bất tử với độ bền lên đến 80 triệu lần nhấn cùng với hiệu ứng ánh sáng RGB 16.8 triệu màu cực kỳ chói lọi, rực rỡ.Tên gọi của em chuột này chính là Gaming Mouse E-Dra EM6102 Pro (thêm chữ Pro để thể hiện tính chất chuyên nghiệp). Đây là phiên bản chuột chơi game có dây thứ 2 trong Series EM6012 và hãng mới chỉ mới đưa ra 1 màu duy nhất là: Black. Vậy E-Dra EM6102 Pro còn ẩn chứa sức mạnh gì đặc biệt trong mức chỉ hơn 300.000 VNĐ nữa. Các bạn hãy cùng An Phát Computer điểm danh chi tiết dưới đây.EM6102 Pro mang thiết kế cơ bản nhưng rất hiệu quả, với vỏ ngoài đối xứng, thuôn nhỏ (giống hệt và giống y như đúc Logitech G102), mang lại cảm giác cầm nắm rất dễ dàng, phù hợp với mọi nhu cầu sử dụng chuột: từ làm việc văn phòng cho tới chơi các thể loại game cơ bản, kể cả là TRY HARD.Bề mặt lớp vỏ của E-Dra EM6102 Pro được phủ một lớp coating chống trượt, nâng cao độ bám tay, mang đến cảm giác cầm chuột và rê chuột hết sức chắc chắn, ngay cả với những người bị mồ hôi.Khác với phiên bản EM6102, E-Dra EM6102 Pro được trang bị cảm biến quang học với DPI tối đa lên tới 10.000. Trong quá trình sử dụng, An Phát Computer nhận thấy chuột cho độ chính xác khá tốt, hoạt động ổn định và tương thích với nhiều bề mặt bàn di cũng như các loại trò chơi thuộc dòng RTS (chiến thuật) như AOE, RPG (nhập vai) như The Witcher 3: Wild Hunt và các game MOBA phổ biến như DOTAII hay LOL.', 2, 8, NULL, CAST(N'2021-05-13T09:55:00.000' AS DateTime), NULL, N'news3.jpg', NULL)
INSERT [dbo].[News] ([NewsID], [Title], [Content], [NewsType], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Image], [Video]) VALUES (4, N'Tổ chức giải bóng đá cho công ty G2Com', N'Công ty G2Com, một công ty kinh doanh về máy tính, vừa thông báo về việc tổ chức một giải bóng đá nội bộ cho các nhân viên của công ty vào cuối tháng này.

Giải bóng đá sẽ diễn ra trong 2 ngày, từ ngày 29 đến ngày 30 tháng này tại sân bóng đá trong khuôn viên của công ty. Sự kiện này nhằm mục đích tăng cường tinh thần đoàn kết và gắn kết giữa các nhân viên trong công ty.

Các đội bóng sẽ được tổ chức theo các bộ phận của công ty, bao gồm bộ phận kinh doanh, bộ phận kỹ thuật và bộ phận quản lý. Mỗi đội sẽ có 7 cầu thủ và 3 người dự bị.

Trước khi bắt đầu giải đấu, Ban tổ chức sẽ tổ chức một buổi họp mặt và rèn luyện kỹ năng cho các cầu thủ. Các cầu thủ cũng sẽ được cung cấp đầy đủ trang phục thi đấu và trang thiết bị an toàn như giày, áo, quần và bảo vệ chân.

Tất cả các trận đấu sẽ được quản lý và giám sát bởi các trọng tài chuyên nghiệp, đảm bảo sự công bằng và minh bạch trong việc đánh giá các bàn thắng và thẻ phạt.

Công ty G2Com hy vọng rằng giải bóng đá nội bộ này sẽ đem lại niềm vui và sự hưng phấn cho các nhân viên của công ty và tạo ra một không khí tích cực và đoàn kết trong công ty.', 1, 1, NULL, CAST(N'2021-05-13T09:55:00.000' AS DateTime), NULL, N'news4.jpg', NULL)
INSERT [dbo].[News] ([NewsID], [Title], [Content], [NewsType], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Image], [Video]) VALUES (5, N'Công ty G2Com triển khai dự án mới về hệ thống quản lý khách hàng', N'Công ty G2Com, một công ty kinh doanh về máy tính, vừa thông báo về việc triển khai dự án mới về hệ thống quản lý khách hàng. Dự án này được thiết kế để cải thiện quản lý và tối ưu hóa quá trình tương tác với khách hàng của công ty.

Theo thông tin từ Ban quản lý dự án, dự án sẽ bao gồm việc triển khai một hệ thống quản lý khách hàng mới hoàn toàn tích hợp với các phần mềm hiện có của công ty. Hệ thống này sẽ cung cấp các tính năng như theo dõi lịch sử mua hàng của khách hàng, tối ưu hóa quá trình bán hàng, quản lý dữ liệu khách hàng và cung cấp các báo cáo phân tích dữ liệu chi tiết để giúp quản lý đưa ra quyết định hiệu quả.

Ngoài ra, dự án cũng sẽ đào tạo nhân viên để sử dụng hệ thống quản lý khách hàng mới và cập nhật các quy trình kinh doanh mới liên quan đến việc quản lý khách hàng. Ban quản lý dự án hy vọng rằng dự án này sẽ giúp công ty G2Com cải thiện khả năng tương tác với khách hàng và tăng cường khả năng cạnh tranh của công ty trên thị trường.

Dự án dự kiến sẽ triển khai trong 6 tháng, với sự tham gia của một đội ngũ chuyên gia IT và các chuyên gia kinh doanh của công ty. Công ty G2Com tin rằng, dự án mới sẽ đem lại lợi ích rất lớn cho công ty, giúp nâng cao chất lượng dịch vụ và đáp ứng tốt hơn nhu cầu của khách hàng.', 1, 2, NULL, CAST(N'2021-05-13T09:55:00.000' AS DateTime), NULL, N' news5.jpg', NULL)
INSERT [dbo].[News] ([NewsID], [Title], [Content], [NewsType], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Image], [Video]) VALUES (6, N'Công ty G2Com kí kết hợp tác cùng đối tác Nhật Bản nhằm nâng cao chất lượng sản phẩm ', N'Công ty G2Com, một công ty kinh doanh máy tính tại Việt Nam, đã thông báo về việc hợp tác với một đối tác Nhật Bản nhằm nâng cao chất lượng sản phẩm của mình.

Theo thông tin được công bố, đối tác Nhật Bản này là một công ty chuyên sản xuất và phân phối linh kiện và thiết bị cho ngành công nghiệp điện tử. Hợp tác giữa G2Com và đối tác Nhật Bản này sẽ tập trung vào việc chia sẻ công nghệ và kiến thức để cải thiện chất lượng sản phẩm của G2Com.

Ông Nguyễn Văn A, Giám đốc điều hành của G2Com, cho biết: "Chúng tôi rất vui mừng về việc hợp tác này và tin rằng nó sẽ giúp cho chúng tôi nâng cao chất lượng sản phẩm và tăng cường năng lực cạnh tranh trên thị trường. Chúng tôi hy vọng rằng đối tác Nhật Bản của chúng tôi sẽ mang lại cho chúng tôi những giá trị mới và cùng đồng hành với chúng tôi trong chiến lược phát triển của công ty."

Hiện tại, hai công ty đang tiến hành các cuộc thảo luận cụ thể để đưa ra các kế hoạch và chiến lược cụ thể cho việc hợp tác. Dự kiến, sự hợp tác này sẽ giúp cho G2Com nâng cao chất lượng sản phẩm, cải thiện quy trình sản xuất và tăng cường năng lực cạnh tranh trên thị trường.', 1, 8, NULL, CAST(N'2021-05-13T09:55:00.000' AS DateTime), NULL, N'news6.jpg', NULL)
SET IDENTITY_INSERT [dbo].[News] OFF
GO
SET IDENTITY_INSERT [dbo].[NewsType] ON 

INSERT [dbo].[NewsType] ([NewsTypeID], [NewsTypeName]) VALUES (1, N'Tin Công Ty')
INSERT [dbo].[NewsType] ([NewsTypeID], [NewsTypeName]) VALUES (2, N'Tin Khách Hàng')
SET IDENTITY_INSERT [dbo].[NewsType] OFF
GO
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([OrderID], [UserID], [OrderDate], [ShipDate], [TotalPrice], [Status], [PaymentType], [ShipAddress]) VALUES (1, 1, CAST(N'2022-02-15T09:55:00.000' AS DateTime), CAST(N'2022-02-17T12:55:00.000' AS DateTime), 34480000.0000, 1, N'Momo', N'Số 17 Ngõ 40 Văn La, Phường Phú La, Quận Hà Đông, Hà Nội')
INSERT [dbo].[Order] ([OrderID], [UserID], [OrderDate], [ShipDate], [TotalPrice], [Status], [PaymentType], [ShipAddress]) VALUES (2, 2, CAST(N'2022-06-21T15:12:00.000' AS DateTime), CAST(N'2022-06-25T10:12:00.000' AS DateTime), 27980000.0000, 1, N'Momo', N'Lô 33, LK5 khu đô thị Tân Tây Đô, Xã Tân Lập, Huyện Đan Phượng, Hà Nội')
INSERT [dbo].[Order] ([OrderID], [UserID], [OrderDate], [ShipDate], [TotalPrice], [Status], [PaymentType], [ShipAddress]) VALUES (3, 3, CAST(N'2022-02-05T16:00:00.000' AS DateTime), CAST(N'2022-02-09T10:12:00.000' AS DateTime), 41970000.0000, 1, N'Momo', N'223 Hoàng Văn Thụ (K3.28 Cao ốc Kingston Residence), Phường 08, Quận Phú Nhuận, TP Hồ Chí Minh')
INSERT [dbo].[Order] ([OrderID], [UserID], [OrderDate], [ShipDate], [TotalPrice], [Status], [PaymentType], [ShipAddress]) VALUES (4, 4, CAST(N'2022-02-15T19:45:00.000' AS DateTime), CAST(N'2022-02-18T10:12:00.000' AS DateTime), 48980000.0000, 1, N'Momo', N'120 Vũ Tông Phan , Khu Phố 5, Phường An Phú, Thành phố Thủ Đức, TP Hồ Chí Minh')
INSERT [dbo].[Order] ([OrderID], [UserID], [OrderDate], [ShipDate], [TotalPrice], [Status], [PaymentType], [ShipAddress]) VALUES (5, 1, CAST(N'2022-05-16T09:55:00.000' AS DateTime), CAST(N'2022-05-18T17:55:00.000' AS DateTime), 45380000.0000, 1, N'Momo', N'Số 17 Ngõ 40 Văn La, Phường Phú La, Quận Hà Đông, Hà Nội')
SET IDENTITY_INSERT [dbo].[Order] OFF
GO
INSERT [dbo].[OrderDetails] ([ProductID], [OrderID], [Quantity], [Price], [Discount]) VALUES (1, 1, 1, 11490000.0000, 0)
INSERT [dbo].[OrderDetails] ([ProductID], [OrderID], [Quantity], [Price], [Discount]) VALUES (1, 3, 2, 22980000.0000, 0)
INSERT [dbo].[OrderDetails] ([ProductID], [OrderID], [Quantity], [Price], [Discount]) VALUES (2, 2, 2, 27980000.0000, 0)
INSERT [dbo].[OrderDetails] ([ProductID], [OrderID], [Quantity], [Price], [Discount]) VALUES (3, 5, 1, 22390000.0000, 0)
INSERT [dbo].[OrderDetails] ([ProductID], [OrderID], [Quantity], [Price], [Discount]) VALUES (4, 1, 1, 22990000.0000, 0)
INSERT [dbo].[OrderDetails] ([ProductID], [OrderID], [Quantity], [Price], [Discount]) VALUES (4, 4, 1, 22990000.0000, 0)
INSERT [dbo].[OrderDetails] ([ProductID], [OrderID], [Quantity], [Price], [Discount]) VALUES (4, 5, 1, 22990000.0000, 0)
INSERT [dbo].[OrderDetails] ([ProductID], [OrderID], [Quantity], [Price], [Discount]) VALUES (5, 4, 1, 25990000.0000, 0)
INSERT [dbo].[OrderDetails] ([ProductID], [OrderID], [Quantity], [Price], [Discount]) VALUES (6, 3, 1, 18990000.0000, 0)
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ProductID], [ProductName], [BrandID], [CategoryID], [Description], [UnitPrice], [Discount], [Img], [UnitInStock], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Guarantee]) VALUES (1, N'Laptop Asus Vivobook 15 X515EA BR2045W', 3, 1, N'Intel Core i3-1115G4 1.7GHz up to 4.1GHz 6MB|4GB Onboard DDR4 2666MHz (1x SO-DIMM socket, up to 12GB SDRAM)|512GB M.2 NVMe™ PCIe® 3.0 SSD, 1x slot SATA3 2.5"|Intel UHD Graphics|15.6" HD (1366 x 768), Anti-glare display, LED Backlit, 200nits, NTSC: 45%, Screen-to-body ratio: 83 ％|Tiêu chuẩn, có phím số', 11490000.0000, 5, N'gearvn-laptop-asus-vivobook-1.webp|gearvn-laptop-asus-vivobook-2.webp|gearvn-laptop-asus-vivobook-3.webp', 30, 3, NULL, CAST(N'2021-02-15T09:55:00.000' AS DateTime), NULL, 24)
INSERT [dbo].[Product] ([ProductID], [ProductName], [BrandID], [CategoryID], [Description], [UnitPrice], [Discount], [Img], [UnitInStock], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Guarantee]) VALUES (2, N'Laptop Asus Vivobook X1404VA NK124W', 3, 1, N'Intel® Core™ i3-1315U Processor 1.2 GHz (10MB Cache, up to 4.5 GHz, 6 cores, 8 Threads)|8GB (Onboard) DDR4 3200MHz (Còn 1 slot SO-DIMM, nâng cấp tối đa 16GB)|256GB M.2 NVMe™ PCIe® 4.0 SSD (1x M.2 2280 PCIe 4.0x4)|Intel® UHD Graphics|14" FHD (1920 x 1080) LED backlit, 60Hz, Anti-glare display ,250nits, 45% NTSC color gamut, Screen-to-body ratio 82%', 13990000.0000, 6, N'nk124w_1.webp|vivobook-14-x1404va-nk124w-2.webp|vivobook-14-x1404va-nk124w-3.webp', 40, 9, NULL, CAST(N'2021-02-11T11:55:00.000' AS DateTime), NULL, 24)
INSERT [dbo].[Product] ([ProductID], [ProductName], [BrandID], [CategoryID], [Description], [UnitPrice], [Discount], [Img], [UnitInStock], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Guarantee]) VALUES (3, N'Laptop Dell Inspiron 15 3520 70296960', 2, 1, N'Intel(R) Core(TM) i5-1235U Processor (12MB Cache, up to 4.4 GHz)|1 x 8GB DDR4 2666MHz (2x SO-DIMM socket, up to 16GB SDRAM)|	512GB SSD NVMe PCIe (1 Slot)|NVIDIA GeForce MX550 2 GB|15.6 Inch FHD (1920 x 1080),120Hz, Anti- Glare, WVA, LED-Backlit, Độ sáng 250 nit', 22390000.0000, 8, N'khung-lt-van-phong_1.webp|in3520-cnb-2.webp|in3520-cnb-3.webp', 45, 9, NULL, CAST(N'2021-01-04T08:45:00.000' AS DateTime), NULL, 24)
INSERT [dbo].[Product] ([ProductID], [ProductName], [BrandID], [CategoryID], [Description], [UnitPrice], [Discount], [Img], [UnitInStock], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Guarantee]) VALUES (4, N'Laptop Dell Inspiron 14 5420 i5U085W11SLU', 2, 1, N'Intel Core i5-1235U Processor (12MB Cache, up to 4.4 GHz)|8GB DDR4 3200MHz (2x SO-DIMM socket, up to 16GB SDRAM)|512GB M.2 PCIe NVMe SSD|Intel UHD Graphics|14.0-inch 16:10 FHD+ (1920 x 1200),60Hz, Anti-Glare Non-Touch 250nits WVA Display with ComfortView Support', 22990000.0000, 10, N'i5u085w11slu_1.webp|notebook-inspiron-2.webp|notebook-inspiron-3.webp', 35, 3, NULL, CAST(N'2021-01-04T13:23:00.000' AS DateTime), NULL, 24)
INSERT [dbo].[Product] ([ProductID], [ProductName], [BrandID], [CategoryID], [Description], [UnitPrice], [Discount], [Img], [UnitInStock], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Guarantee]) VALUES (5, N'Laptop Lenovo ThinkPad T14 Gen2 20W0016EVA', 5, 1, N'Intel® Core™ i5-1135G7 (4 Cores  8 Threads , 2.4 / 4.2GHz, 8MB)|8GB Soldered DDR4-3200 (Trống 1 slot Sodimm, nâng cấp tối đa 40GB)|512GB SSD M.2 2280 PCIe® 4.0x4 NVMe® Opal 2.0 (Còn trống 1 Slot M.2 2242 PCIe 3.0 x4)|	Intel Iris Xe Graphics (with dual channel memory)|14" FHD (1920x1080) IPS 300nits Anti-glare, 45% NTSC', 25990000.0000, 13, N'20w0016eva-1.webp|thinkpad_t14_gen_2.webp|thinkpad_t14_3.webp', 25, 3, NULL, CAST(N'2021-11-12T10:02:00.000' AS DateTime), NULL, 24)
INSERT [dbo].[Product] ([ProductID], [ProductName], [BrandID], [CategoryID], [Description], [UnitPrice], [Discount], [Img], [UnitInStock], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Guarantee]) VALUES (6, N'Laptop Lenovo IdeaPad 1 15AMN7 82VG0022VN', 5, 1, N'AMD Ryzen™ 5 7520U (4 Cores  8 Threads, 2.8GHz up to 4.3GHz, 2MB L2 / 4MB L3)|8GB Soldered LPDDR5-5500 (Không nâng cấp)|512GB SSD M.2 2242 PCIe® 4.0x4 NVMe®|Integrated AMD Radeon™ 610M Graphics|15.6" FHD (1920x1080) TN 220nits Anti-glare', 18990000.0000, 5, N'gearvn-laptop-lenovo-ideapad-1.webp|gearvn-laptop-lenovo-ideapad-2.webp|gearvn-laptop-lenovo-ideapad-3.webp', 23, 3, NULL, CAST(N'2021-11-12T10:05:00.000' AS DateTime), NULL, 24)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[RepairBill] ON 

INSERT [dbo].[RepairBill] ([BillID], [UserID], [StartDate], [EndDate], [TotalPrice], [Address]) VALUES (1, 1, CAST(N'2023-04-12T11:50:00.000' AS DateTime), CAST(N'2023-04-16T15:45:00.000' AS DateTime), 150000.0000, N' Số 16A ngách 12 ngõ 67 Tô Ngọc Vân, Phường Quảng An, Quận Tây Hồ, Hà Nội')
INSERT [dbo].[RepairBill] ([BillID], [UserID], [StartDate], [EndDate], [TotalPrice], [Address]) VALUES (2, 3, CAST(N'2023-03-14T08:50:00.000' AS DateTime), CAST(N'2023-03-17T15:45:00.000' AS DateTime), 180000.0000, N'462 Nguyễn Thị Định, Phường Thạnh Mỹ Lợi, Thành phố Thủ Đức, TP Hồ Chí Minh')
INSERT [dbo].[RepairBill] ([BillID], [UserID], [StartDate], [EndDate], [TotalPrice], [Address]) VALUES (3, 4, CAST(N'2023-03-16T08:50:00.000' AS DateTime), CAST(N'2023-03-20T15:45:00.000' AS DateTime), 180000.0000, N'462 Nguyễn Thị Định, Phường Thạnh Mỹ Lợi, Thành phố Thủ Đức, TP Hồ Chí Minh')
SET IDENTITY_INSERT [dbo].[RepairBill] OFF
GO
INSERT [dbo].[RepairBillDetails] ([BillID], [ProductID], [ErrorID], [Quantity], [Price]) VALUES (1, 1, 1, 1, 80000.0000)
INSERT [dbo].[RepairBillDetails] ([BillID], [ProductID], [ErrorID], [Quantity], [Price]) VALUES (2, 1, 8, 1, 100000.0000)
INSERT [dbo].[RepairBillDetails] ([BillID], [ProductID], [ErrorID], [Quantity], [Price]) VALUES (1, 4, 3, 1, 70000.0000)
INSERT [dbo].[RepairBillDetails] ([BillID], [ProductID], [ErrorID], [Quantity], [Price]) VALUES (3, 4, 8, 1, 100000.0000)
INSERT [dbo].[RepairBillDetails] ([BillID], [ProductID], [ErrorID], [Quantity], [Price]) VALUES (3, 5, 1, 1, 80000.0000)
INSERT [dbo].[RepairBillDetails] ([BillID], [ProductID], [ErrorID], [Quantity], [Price]) VALUES (2, 6, 1, 1, 80000.0000)
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [UserName], [Address], [Phone], [Email], [Password]) VALUES (1, N'Nguyễn Thị T', N'Số 17 Ngõ 40 Văn La, Phường Phú La, Quận Hà Đông, Hà Nội', N'0987463557', N'user123@gmail.com', N'123456')
INSERT [dbo].[User] ([UserID], [UserName], [Address], [Phone], [Email], [Password]) VALUES (2, N'Nguyễn Văn C', N'Lô 33, LK5 khu đô thị Tân Tây Đô, Xã Tân Lập, Huyện Đan Phượng, Hà Nội', N'0897465273', N'haha333@gmail.com', N'654321')
INSERT [dbo].[User] ([UserID], [UserName], [Address], [Phone], [Email], [Password]) VALUES (3, N'Phạm Văn K', N'223 Hoàng Văn Thụ (K3.28 Cao ốc Kingston Residence), Phường 08, Quận Phú Nhuận, TP Hồ Chí Minh', N'0987654321', N'nui123@gmail.com', N'211321')
INSERT [dbo].[User] ([UserID], [UserName], [Address], [Phone], [Email], [Password]) VALUES (4, N'Nguyễn Lê Bảo Vy', N'120 Vũ Tông Phan , Khu Phố 5, Phường An Phú, Thành phố Thủ Đức, TP Hồ Chí Minh', N'0123456789', N'bien321@gmail.com', N'567890')
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[Admin]  WITH CHECK ADD FOREIGN KEY([RoleID])
REFERENCES [dbo].[AdminRole] ([RoleID])
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[Guarantee]  WITH CHECK ADD FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[Guarantee]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Admin] ([AdminID])
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Admin] ([AdminID])
GO
ALTER TABLE [dbo].[News]  WITH CHECK ADD FOREIGN KEY([NewsType])
REFERENCES [dbo].[NewsType] ([NewsTypeID])
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD FOREIGN KEY([OrderID])
REFERENCES [dbo].[Order] ([OrderID])
GO
ALTER TABLE [dbo].[OrderDetails]  WITH CHECK ADD FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([BrandID])
REFERENCES [dbo].[Brand] ([BrandID])
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([CategoryID])
REFERENCES [dbo].[Category] ([CategoryID])
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[Admin] ([AdminID])
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[Admin] ([AdminID])
GO
ALTER TABLE [dbo].[RepairBill]  WITH CHECK ADD FOREIGN KEY([UserID])
REFERENCES [dbo].[User] ([UserID])
GO
ALTER TABLE [dbo].[RepairBillDetails]  WITH CHECK ADD FOREIGN KEY([BillID])
REFERENCES [dbo].[RepairBill] ([BillID])
GO
ALTER TABLE [dbo].[RepairBillDetails]  WITH CHECK ADD FOREIGN KEY([ErrorID])
REFERENCES [dbo].[Error] ([ErrorID])
GO
ALTER TABLE [dbo].[RepairBillDetails]  WITH CHECK ADD FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
USE [master]
GO
ALTER DATABASE [Computer_Shop] SET  READ_WRITE 
GO
